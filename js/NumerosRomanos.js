
/* 
 * Descripción : Metodo que corvierto el numero romano a numero natural
 * Realizado : Diego Castellanos
 * Fecha de creación : 14 de Sep de 2014
 * Proyecto : viveLab
*/

function convertirNumeroRomanoANatural(numRomano) 
{

    // Se define array con las letras validas
    var arrayRim = ['M', 'D', 'C', 'L', 'X', 'V', 'I'];

    //Equivalente de las letras validas
    var arrayDec = [1000, 500, 100, 50, 10, 5, 1];

    var strRim = "";
    var strNumeroRomano = "";
    var intDec = 0;
    var intArap = 0;
    var tempRim = 0;

    strNumeroRomano = numRomano;

    //Convertir a mayuscula
    strNumeroRomano = strNumeroRomano.toUpperCase();

    //Valido que se ha una letra
    for (var i=0; i<strNumeroRomano.length; i++) 
    {

        for (var j=0; j<arrayRim.length; j++) 
        {
            
            if (arrayRim[j] == strNumeroRomano.charAt(i)) 
            {
                intArap = arrayDec[j]; break;
            }
            
            intArap = 0;
        }

        if (intArap == 0) 
        {
            alert ('Señor usuario/a, S\u00F3lo las letras IVXLCDM est\u00E1n permitidas, reintenta por favor.');
            return true;
        }

        if (tempRim < intArap) 
        {
            tempRim *= -1
        }

        intDec += tempRim;
        tempRim = intArap;
    }

    intDec += tempRim;

    return intDec;

}
// ***********************************************

/* 
 * Descripción : Metodo que corvierto el numero natural a romano
 * Realizado : Diego Castellanos
 * Fecha de creación : 14 de Sep de 2014
 * Proyecto : viveLab
*/

function convertirNumeroNaturarARomano(numNatural)
{

    var arrayRim = ['M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I'];
    var arrayDec = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
    var intDec = 0;
    var strNumeroNatural = "";

    intDec = numNatural; 
    
    for (var i=0; i<arrayRim.length; i++) 
    {
        while (intDec >= arrayDec[i]) 
        {
            strNumeroNatural += arrayRim[i];
            intDec -= arrayDec[i];
        }
    }
    
    return strNumeroNatural;

}