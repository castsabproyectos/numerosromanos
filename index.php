<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> 

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />    
    
<head>
<title>N&uacute;meros Romanos - Conversi&oacute;n</title>
</head>

<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery_validate.js"></script>
<script type="text/javascript" src="js/NumerosRomanos.js"></script>

<link rel="stylesheet" type="text/css" href="css/formulario.css"></link>

<script type="text/javascript">

    $(document).ready(function(){
        
        $('#frmSumarNumeros').validate({
            
            rules:
            {
                txtNumeroRomano1: {required:true,lettersonly:true},
                txtNumeroRomano2: {required:true,lettersonly:true}
            },
            
            submitHandler: function()
            {
                
                var txtNumeroRomano1 = '';
                var txtNumeroRomano2 = '';
                var resultado = '';
                
                txtNumeroRomano1 = $('#txtNumeroRomano1').val();
                txtNumeroRomano2 = $('#txtNumeroRomano2').val();
                
                //alert('txtNumeroRomano1 : '+txtNumeroRomano1+' - txtNumeroRomano2 : '+txtNumeroRomano2);
                
                txtNumeroRomano1 = convertirNumeroRomanoANatural(txtNumeroRomano1);
                txtNumeroRomano2 = convertirNumeroRomanoANatural(txtNumeroRomano2);
                
                //alert('txtNumeroRomano1 : '+txtNumeroRomano1+' - txtNumeroRomano2 : '+txtNumeroRomano2);
                
                if(txtNumeroRomano1 == true || txtNumeroRomano2 == true)
                {
                    limpiarFormulario();
                    return false;
                }
                else
                {
                    resultado = (txtNumeroRomano1 + txtNumeroRomano2);
                    resultado = convertirNumeroNaturarARomano(resultado);
                    $('#txtResultado').text('Resultado : '+resultado);
                }
            }
        });
        
        $('#btnLimpiar').click(function()
        {
            limpiarFormulario();
        });
        
    });
    
    function limpiarFormulario()
    {
        $('#txtNumeroRomano1').val("");
        $('#txtNumeroRomano2').val("");
        $('#txtResultado').text("");
    }

</script>

<body>

<div id="formulario" align="center">

    <h1>Suma de numeros romanos</h1>    
    
    <form id="frmSumarNumeros" name="frmSumarNumeros">
        
        <table>
            
            <tr>
                <td><input type="text" id="txtNumeroRomano1" name="txtNumeroRomano1" value=""></td>
            </tr>
            
            <tr>
                <td><input type="text" id="txtNumeroRomano2" name="txtNumeroRomano2" value=""></td>
            </tr>
            
            <tr>
                <td>
                    <label style="font-weight:bold" id="txtResultado" name="txtResultado"></label>
                </td>
            </tr>
            
            <tr>
                <td>
                    <input id="btnSumar" name="btnSumar" type="submit" value="+ Sumar"></input>
                </td>
            </tr>
        
        </table>
        
    </form>
    
    <input id="btnLimpiar" name="btnLimpiar" type="submit" value="Limpiar"></input>

</div>

</body>

</html>


      


